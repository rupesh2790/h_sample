<html ng-app="myapp">
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<!-- <script src="/JS/angular.min.js"></script> -->
<!-- <script src="JS/angular.js"></script> -->
<script>
	var app = angular.module("myapp", []);
	app.controller("mycon",function($scope,$http){
	
		$scope.bookform={id:null, title:"",author:""};
              
		details();
		
		function details(){
			 $http({
			       method:"GET",
			        url:"book"
			    }).then(function(response){
			    	
			    	 $scope.book=response.data;
			    	 console.log(response);
			    	 console.log(response.status);
			    });
			}
		
         $scope.processBook=function(){
             alert("hi");
        	 $http({
			       method:"POST",
			        url:"book",
			        data : angular.toJson($scope.bookform),
			        headers : {
						'Content-Type' : 'application/json'
					}
			    }).then(function(){
			    	details();
			    	clearForm();
	 	 		    });
             } 

         $scope.editBook= function(a) {
        	 $scope.bookform.id=a.id;
 			$scope.bookform.title=a.title;
 			$scope.bookform.author=a.author;
 			disableName();
 			
 		}
         $scope.deleteBook= function(a) {
 			$http({
 				method : 'DELETE',
 				url : 'deletebook/',
 				params:{id:a.id},
 				data : angular.toJson(a),
 				headers : {
 					'Content-Type' : 'application/json'
 				}
 			}).then(function(){
 				details();
 				});
 		}


     	function clearForm() {
			$scope.bookform.id = "";
			$scope.bookform.title = "";
			$scope.bookform.author = "";
			document.getElementById("id").disabled = false;
		}
		function disableName() {
			document.getElementById("id").disabled = true;
		} 
       });
	
	</script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

</head>
<body ng-controller="mycon">
	<!-- <center> -->
	<div style="height: 1000px; width: 1200px;" align="center">
		<div style="background-color: navy;">
			<h1 style="color: white;">WELCOME TO HERE</h1>
		</div>

		<h2 style="color: red;">Book Registration Form</h2>
		<form>
			<div class="table-responsive">
				<table class="table table-bordered" style="width: 600px">
					<tr>
						<td>Book ID</td>
						<td><input type="text" id="id" ng-model="bookform.id"
							size="30" disabled="disabled" /></td>
					</tr>

					<tr>
						<td>Book Title</td>
						<td><input type="text" id="title" ng-model="bookform.title"
							size="30" /></td>
					</tr>
					<tr>
						<td>Book Author</td>
						<td><input type="text" id="author" ng-model="bookform.author"
							size="30" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit"
							class="btn btn-primary btn-sm" ng-click="processBook()"
							value="Create / Update Book" /></td>
					</tr>
				</table>
			</div>
		</form>
		<h2 style="color: blue;">Table Data</h2>
		<div class="table-responsive">
			<table class="table table-bordered" style="width: 600px">
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Author</th>
					<th>Action</th>
				</tr>
				<tr ng-repeat="a in book">
					<td>{{a.id}}</td>
					<td>{{a.title}}</td>
					<td>{{a.author}}</td>
					<td><a ng-click="editBook(a)" class="btn btn-primary btn-sm">Edit</a>
						| <a ng-click="deleteBook(a)" class="btn btn-danger btn-sm">Delete</a></td>

				</tr>

			</table>
		</div>

	</div>
	<!-- </center>  -->

</body>
</html>
