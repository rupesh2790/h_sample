<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="1">
<tr><td colspan="1">LIST OF BOOKS</td></tr>
<tr><th>ID</th>
<th>TITLE</th>
<th>AUTHOR</th>
<th>EDIT</th>
<th>DELETE</th>
</tr>
<c:forEach items="${bookdata}" var="b">
<tr>
<td><c:out value="${b.id}"/></td>
<td><c:out value="${b.title}"/></td>
<td><c:out value="${b.author}"/></td>
<td><a href="edit.html?id=${b.id}">EDIT</a></td>
<td><a href="delete.html?id=${b.id}">DELETE</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>