package com.Dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Distinct;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.model.Book;
@javax.transaction.Transactional
@Repository
public class BookDaoImpl implements BookDao{

	@Autowired
private	SessionFactory sessionFactory;
	
	public long save(Book book) {
		
		sessionFactory.getCurrentSession().saveOrUpdate(book);
		return book.getId();
	}

	public Book get(long id) {
		
		Session session=sessionFactory.getCurrentSession();
		Book book=(Book)session.get(Book.class, id);
		
		return book;
		
	}

	public List<Book> list() {
		
		Session session=sessionFactory.getCurrentSession();
		List<Book> list=session.createCriteria(Book.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		return list;
	}

	public void update(long id, Book book) {
		
		Session session=sessionFactory.getCurrentSession();
		Book book2=(Book) session.byId(Book.class).load(id);
		book2.setTitle(book.getTitle());
		book2.setAuthor(book.getAuthor());
		session.flush();
	}

	public void delete(long id) {

		Session session=sessionFactory.getCurrentSession();
		Book book=(Book) session.byId(Book.class).load(id);
		session.delete(book);
	}

	@Override
	public void deleteBook(Book bk) {
		Session session=sessionFactory.getCurrentSession();
		session.delete(bk);
	}

}
