package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Dao.BookDao;
import com.model.Book;

@Service
public class BookServiceImpl implements bookService{

	@Autowired
	BookDao dao;
	
	public long save(Book book) {
		
		return dao.save(book);
	}

	public Book get(long id) {
		
		return dao.get(id);
	}

	public List<Book> list() {
		
		return dao.list();
	}

	public void update(long id, Book book) {
		dao.update(id, book);
		
	}

	public void delete(long id) {
		
		dao.delete(id);
	}

	@Override
	public void deleteBook(Book bk) {
		dao.deleteBook(bk);
	}

	
	
	
}
