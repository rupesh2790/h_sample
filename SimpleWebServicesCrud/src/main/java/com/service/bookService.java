package com.service;

import java.util.List;

import com.model.Book;



public interface bookService {

	long save (Book book);
	Book get(long id);
	List<Book> list();
	void update(long id,Book book);
	void delete(long id);
	void deleteBook(Book bk);
}
