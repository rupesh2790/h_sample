package com.controller;

import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.model.Book;
import com.service.bookService;


@RestController
public class BookController {

	@Autowired
	bookService service;
	
	@RequestMapping(value="/register",method=RequestMethod.GET)
    @RolesAllowed("ADMIN")
	
	@GetMapping("/register")
	public ModelAndView go()
	{
		RestTemplate restTemplate=new RestTemplate();
		String url="http://localhost:8092/SimpleWebServicesCrud/";
		Book book=restTemplate.getForObject(url,Book.class);
		return new ModelAndView("registerBook","bookclass",book);
	}
	
	@PostMapping("/book")
	public ResponseEntity<?> save(@RequestBody Book book)
	{
		long id=service.save(book);
		
		return ResponseEntity.ok().body("New Book Saved="+id);
	}
	
	
	@GetMapping("/book/{id}")
	public ResponseEntity<Book> get(@PathVariable("id") long id)
	{
		Book book=service.get(id);
		return ResponseEntity.ok().body(book);
	}
	
	@GetMapping("/book")
	public ResponseEntity<List<Book>> list()
	{
		List<Book> list1=service.list();
		return ResponseEntity.ok().body(list1);
	}
	/*@GetMapping("/book")
	public ModelAndView list()
	{
		RestTemplate restTemplate=new RestTemplate();
		String url="http://localhost:8092/SimpleWebServicesCrud/";
		
		//List<Book> list1=service.list();
		List<Book> li=(List<Book>) restTemplate.getForObject(url,Book.class, service.list());
		return new ModelAndView("registerBooks","bookdata",li);
	}*/
	@PutMapping("/book/{id}")
	public ResponseEntity<?> update(@PathVariable("id")long id,@RequestBody Book book)
	{
		service.update(id, book);
		return ResponseEntity.ok().body("Book has been updated succesfully........");
	}
	
	@DeleteMapping("/deletebook/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") String s,@RequestBody Book book)
	{
		long id=Long.parseLong(s);
		service.delete(id);
		return ResponseEntity.ok().body("Deleted..........");
	}
	@DeleteMapping("/deletebook")
	public void deleteBook(@RequestBody Book book)
	{
		
		service.deleteBook(book);
	}
}
